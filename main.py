#coding: utf-8
import datetime
import hashlib
from flask import Flask, render_template, request, redirect, abort, session, g, json
from flask.helpers import url_for
from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy import or_, not_, and_, func
from sqlalchemy.orm import joinedload
from config.config import STATIC_PATH, SQLALCHEMY_DATABASE_URI, SQLALCHEMY_MIGRATE_REPO
from flask.ext.sqlalchemy import SQLAlchemy

app = Flask(__name__, static_folder=STATIC_PATH)
app.config['SQLALCHEMY_DATABASE_URI'] = SQLALCHEMY_DATABASE_URI
app.config['SQLALCHEMY_MIGRATE_REPO'] = SQLALCHEMY_MIGRATE_REPO
app.secret_key = '\xa1\xe8q6\xd9\xfb\x95\xb0\xb1a\xe4\xe8\x8c-z\x7f\x87\xfb?\xbb\x9c\x13Y\x10'
db = SQLAlchemy(app)

import models


@app.before_request
def before_request():
    if 'session_key' in session:
        try:
            user_session = db.session.query(models.Session).filter(models.Session.session_key == session['session_key']).one()
            g.user = user_session.user
        except NoResultFound:
            session.pop('session_key', None)


@app.route("/")
def index():
    return render_template('index.html')


@app.route('/hello-user/<string:user>/')
def hello_user(user):
    title = u'Привет внутрь этой небольшой функции'
    return render_template('hello_user.html', title=title, user=user)


@app.route('/what-time-is-it/')
def what_time_is_it():
    current_date = datetime.datetime.now()
    a = [i**2 for i in range(1, 11)]
    b = [i**2 for i in range(11, 21)]
    return render_template('what_time_is_it.html', current_date=current_date, a=a, b=b)


@app.route('/implement-template/')
def implement_template():
    return render_template('implement_template.html')


@app.route('/create-student/', methods=['POST', 'GET'])
def create_student():
    form = {
        'fio': '',
        'course': 1,
        'id_group': None,
        'errors': {},
        'valid': True
    }
    groups = db.session.query(models.Group).all()
    if request.method == 'POST':
        if 'fio' in request.form:
            form['fio'] = request.form['fio']
            if len(form['fio']) == 0:
                form['errors']['fio'] = u'Обязательное поле'
                form['valid'] = False
        else:
            form['valid'] = False
            form['errors']['fio'] = u'Обязательное поле'

        if 'course' in request.form:
            form['course'] = request.form['course']
            if len(form['course']) == 0:
                form['errors']['course'] = u'Обязательное поле'
                form['valid'] = False
            else:
                try:
                    form['course'] = int(form['course'])
                except ValueError:
                    form['errors']['course'] = u'Значение не целого типа'
                    form['valid'] = False
        else:
            form['valid'] = False
            form['errors']['course'] = u'Обязательное поле'

        if 'id_group' in request.form:
            form['id_group'] = request.form['id_group']
            if len(form['id_group']) == 0:
                form['errors']['id_group'] = u'Обязательное поле'
                form['valid'] = False
            else:
                try:
                    form['id_group'] = int(form['id_group'])
                except ValueError:
                    form['errors']['id_group'] = u'Значение не целого типа'
                    form['valid'] = False
        else:
            form['valid'] = False
            form['errors']['id_group'] = u'Обязательное поле'

        if form['valid']:
            student = models.Student(
                id_group=form['id_group'],
                fio=form['fio'],
                course=form['course']
            )
            db.session.add(student)
            db.session.commit()
            return redirect(url_for('get_all_students')), 302
    return render_template('create_student.html', form=form, groups=groups)


@app.route('/edit-student/<id_student>/', methods=['POST', 'GET'])
def edit_student(id_student):
    try:
        student = db.session.query(models.Student).filter(models.Student.id == id_student).one()
    except NoResultFound:
        raise abort(404)
    form = {
        'fio': '',
        'course': 1,
        'errors': {},
        'valid': True
    }
    if request.method == 'POST':
        if 'fio' in request.form:
            form['fio'] = request.form['fio']
            if len(form['fio']) == 0:
                form['errors']['fio'] = u'Обязательное поле'
                form['valid'] = False
        else:
            form['valid'] = False
            form['errors']['fio'] = u'Обязательное поле'

        if 'course' in request.form:
            form['course'] = request.form['course']
            if len(form['course']) == 0:
                form['errors']['course'] = u'Обязательное поле'
                form['valid'] = False
            else:
                try:
                    form['course'] = int(form['course'])
                except ValueError:
                    form['errors']['course'] = u'Значение не целого типа'
                    form['valid'] = False
        else:
            form['valid'] = False
            form['errors']['course'] = u'Обязательное поле'

        if form['valid']:
            student.fio = form['fio']
            student.course = form['course']
            db.session.commit()
            return redirect(url_for('get_all_students')), 302
    else:
        form['fio'] = student.fio
        form['course'] = student.course
    return render_template('edit_student.html', form=form, student=student)


@app.route('/get-all-students/')
def get_all_students():
    students = db.session.query(models.Student).options(joinedload('group')).limit(2).all()
    count = db.session.query(func.count(models.Student.id)).scalar()
    return render_template('get_all_students.html', students=students, count=count, len=len)


@app.route('/get-all-groups/')
def get_all_groups():
    groups = db.session.query(models.Group).all()
    return render_template('get_all_groups.html', groups=groups)


@app.route('/create-group/', methods=['POST', 'GET'])
def create_group():
    form = {
        'name': '',
        'errors': {},
        'valid': True
    }
    if request.method == 'POST':
        if 'name' in request.form:
            form['name'] = request.form['name']
            if len(form['name']) == 0:
                form['errors']['name'] = u'Обязательное поле'
                form['valid'] = False
        else:
            form['valid'] = False
            form['errors']['name'] = u'Обязательное поле'

        if form['valid']:
            group = models.Group(
                name=form['name'],
            )
            db.session.add(group)
            db.session.commit()
            return redirect(url_for('get_all_groups')), 302
    return render_template('create_group.html', form=form)


@app.route('/edit-group/<int:id_group>/', methods=['POST', 'GET'])
def edit_group(id_group):
    try:
        group = db.session.query(models.Group).filter(models.Group.id == id_group).one()
    except NoResultFound:
        raise abort(404)
    form = {
        'name': '',
        'errors': {},
        'valid': True
    }
    if request.method == 'POST':
        if 'name' in request.form:
            form['name'] = request.form['name']
            if len(form['name']) == 0:
                form['errors']['name'] = u'Обязательное поле'
                form['valid'] = False
        else:
            form['valid'] = False
            form['errors']['name'] = u'Обязательное поле'

        if form['valid']:
            group.name = form['name']
            db.session.commit()
            return redirect(url_for('get_all_groups')), 302
    else:
        form['name'] = group.name
    return render_template('create_group.html', form=form)


@app.route('/get-more-students/', methods=['POST'])
def get_more_students():
    offset = int(request.form['offset'])
    count = int(request.form['count'])
    students = db.session.query(models.Student).options(joinedload('group')).offset(offset).limit(count).all()
    rows = [
        {
            'url': url_for('edit_student', id_student=x.id),
            'fio': x.fio,
            'group': x.group.name if x.group else '',
            'course': x.course
        } for x in students
    ]
    students_count = db.session.query(func.count(models.Student.id)).scalar()
    return json.dumps({
        'rows': rows,
        'students_count': students_count
    })


@app.route('/login/', methods=['POST', 'GET'])
def login():
    form = {
        'login': '',
        'password': '',
        'errors': {},
        'valid': True
    }
    if request.method == 'POST':
        if 'login' in request.form:
            form['login'] = request.form['login']
            if len(form['login']) == 0:
                form['errors']['login'] = u'Обязательное поле'
                form['valid'] = False
        else:
            form['valid'] = False
            form['errors']['login'] = u'Обязательное поле'
        if 'password' in request.form:
            form['password'] = request.form['password']
            if len(form['password']) == 0:
                form['errors']['password'] = u'Обязательное поле'
                form['valid'] = False
        else:
            form['valid'] = False
            form['errors']['password'] = u'Обязательное поле'

        if form['valid']:
            try:
                user = db.session.query(models.User).filter(models.User.login == form['login']).one()
            except NoResultFound:
                form['valid'] = False
                form['errors']['login'] = u'Пользователь не найден'

        if form['valid'] and not user.check_password(form['password']):
            form['valid'] = False
            form['errors']['password'] = u'Неккоректный пароль'

        if form['valid']:
            session_key = hashlib.sha512(u'%s - %s' % (datetime.datetime.now(), user.id)).hexdigest()
            new_session = models.Session(
                id_user=user.id,
                date_add=datetime.datetime.now(),
                session_key=session_key
            )
            db.session.add(new_session)
            db.session.commit()
            session['session_key'] = session_key
            return redirect(url_for('index')), 302

    return render_template('login.html', form=form)


@app.route('/logout/')
def logout():
    session.pop('session_key', None)
    return redirect(url_for('index')), 302


@app.route('/get-students-count/')
def get_students_count():
    count = len(db.session.query(models.Student).all())
    return json.dumps({'count': count})


if __name__ == "__main__":
    app.run(debug=True)