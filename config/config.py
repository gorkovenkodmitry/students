#coding: utf-8
import os
ROOT_PATH = os.path.dirname(os.path.dirname(__file__))
STATIC_PATH = os.path.join(ROOT_PATH, 'static')

SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(ROOT_PATH, 'students.db')
#mysql
SQLALCHEMY_DATABASE_URI = 'mysql://root:@127.0.0.1:3306/students'
SQLALCHEMY_MIGRATE_REPO = os.path.join(ROOT_PATH, 'db_repository')