#coding: utf-8
import hashlib
import datetime
from jinja2 import defaults
from main import db


class Group(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255))

    def __unicode__(self):
        return u'%s' % self.name


class Student(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    id_group = db.Column(db.Integer, db.ForeignKey(Group.id), nullable=True)
    fio = db.Column(db.String(255))
    course = db.Column(db.Integer, default=1)

    group = db.relationship(Group)

    def __unicode__(self):
        return u'%s' % self.fio


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    login = db.Column(db.String(255), unique=True)
    password = db.Column(db.String(255))

    def __unicode__(self):
        return u'%s' % self.login

    def set_password(self, password):
        self.password = hashlib.sha256(password).hexdigest()

    def check_password(self, password):
        return self.password == hashlib.sha256(password).hexdigest()


class Session(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    session_key = db.Column(db.String(255), unique=True)
    id_user = db.Column(db.Integer, db.ForeignKey(User.id))
    date_add = db.Column(db.DateTime)

    user = db.relationship(User)

    def __unicode__(self):
        return u'%s' % self.id