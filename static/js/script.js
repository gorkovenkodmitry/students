$(document).ready(function () {
    $(document).on('click', 'a#get_students_count', function(e) {
        e.preventDefault();
        var btn = $(this);
        $.ajax({
            url: btn.attr('href'),
            type: 'GET',
            data: '',
            success: function(data) {
                if (typeof(data) != 'object') {
                    var obj = jQuery.parseJSON(data);
                }
                else {
                    var obj = data;
                }
                alert('Общее число студентов: ' + obj.count);
            }
        });
    });

    $(document).on('click', 'a#get_more_students', function(e) {
        e.preventDefault();
        var btn = $(this);
        var offset = parseInt(btn.attr('data-offset'));
        var count = parseInt(btn.attr('data-count'));
        $.ajax({
            url: btn.attr('href'),
            type: 'POST',
            data: 'offset='+offset + '&count='+count,
            success: function(data) {
                if (typeof(data) != 'object') {
                    var obj = jQuery.parseJSON(data);
                }
                else {
                    var obj = data;
                }
                btn.attr('data-offset').text(offset + count);
//                btn.attr('data-count').text(offset + count + );
                console.log(obj);
            }
        });
    });
});